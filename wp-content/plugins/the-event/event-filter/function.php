<?php if( ! defined( 'ABSPATH' )) exit;


add_shortcode('event_filterable_post','the_event_event_filterable_content');

if(!function_exists('the_event_event_filterable_content')) {
function the_event_event_filterable_content() { ?>
	
   <?php $event_cats = the_event_post_categories(); //print_r($event_cats);
   if(count($event_cats)>0) { ?>
   	<div class="event-filterable-wrapper">
   		<div class="container">
   			<div class="row">
   				<div class="col-md-3">
                  <div class="event-filter-option">
                     <div class="event-type-filter">
         					<div class="type-filter">Event type</div>

                        <?php foreach($event_cats as $event_cat) { ?>
                           <label for="<?php  echo esc_attr($event_cat['slug']); ?>"><?php  echo esc_html($event_cat['name']); ?></label>
                           <input type="radio" id="<?php  echo esc_attr($event_cat['slug']); ?>" name="event_type" class="type_dot" value="<?php  echo esc_attr($event_cat['slug']); ?>">
                        <?php } ?>
                     </div>
                     <div class="event-month-filter">
                        <div class="month-filter">Date</div>
                        <?php $months = the_event_post_month_filter(); //print_r($months); 
                        foreach($months as $month) { ?>
                           <label for="<?php echo esc_attr($month['name']); ?>"><?php echo esc_html($month['name']); ?></label>
                           <input type="radio" id="<?php echo esc_attr($month['name']); ?>" name="event_month" class="month_dot" value="<?php echo esc_attr($month['month']); ?>">
                        <?php } ?>
                     </div>
   					</div>
   				</div>
   				<div class="col-md-9">
   					<div class="event-display">
   						<?php echo do_shortcode('[event_all_post]'); ?>
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
   <?php }
}
}


add_action('wp_ajax_event_fetch' , 'the_event_fetch_filter_data');
add_action('wp_ajax_nopriv_event_fetch','the_event_fetch_filter_data');
function the_event_fetch_filter_data(){
   if( ! empty($_POST['event_type'])) {
      $event_type = $_POST['event_type'];
   }
   if( ! empty($_POST['event_month'])) {
      $event_month = $_POST['event_month'];
   }

   $message_data = the_event_filter_data($event_type,$event_month);

   echo json_encode(
      array(
         'event_filter_data' => $message_data,
      )
   );

   wp_die();

}


if(!function_exists('the_event_filter_data')) {
function the_event_filter_data($event_type,$event_month) {
   ob_start();

   $args = array(
      'post_type' => 'event',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'tax_query' => array(
         array(
            'taxonomy' => 'event-type',
            'field' => 'slug',
            'terms' => $event_type,
         ),
      ),
   );
   $query = new WP_Query($args);
   if($query->have_posts()) : ?>
      <div class="event-single-wrap">
         <?php while($query->have_posts()) : $query->the_post(); 
            global $post; $eventID = $post->ID; 
            $event_start_date = get_post_meta($eventID,'event_start',true); 
            $full_date = strtotime($event_start_date);
            $month = date("m",$full_date); 
            // echo $event_month.' ';
            if($event_month) { $search_month = array($event_month); }
            else { $search_month = array(1,2,3,4,5,6,7,8,9,10,11,12); }
            if(in_array($month, $search_month)) { ?>
            <div class="event-single">
               <div class="event-single-image">
                  <a href="<?php the_permalink(); ?>">
                     <?php if(has_post_thumbnail()) { ?>
                        <?php the_post_thumbnail('medium_large'); ?>
                     <?php } else { ?>
                        <!-- <img src="<?php echo esc_url(plugins_url( '/assets/images/event-placeholder.jpg', __FILE__ )); ?>" > -->
                        <img src="<?php echo THE_EVENT_DEFAULT_EVENT_IMG; ?>" >
                     <?php } ?>
                  </a>
               </div>
               <div class="event-single-description">
                  <div class="event-single-title">
                     <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                  </div>
                  <div class="event-single-desc">
                     <?php the_content(); ?>
                  </div>
               </div>
            </div>
         <?php } endwhile; ?>
      </div>
   <?php wp_reset_postdata(); 
   else:
      echo '<div class="event-none-notice"><h4>No events.</h4></div>';
   endif;

   return ob_get_clean();
}
}


add_shortcode('event_all_post','the_event_default_post');

if(!function_exists('the_event_default_post')) {
function the_event_default_post() {
   $event_args = array(
      'post_type' => 'event',
      'post_status' => 'publish',
      'posts_per_page' => -1,
   );
   $event_query = new WP_Query($event_args);
   if($event_query->have_posts()) : ?>
      <div class="event-single-wrap">
         <?php while($event_query->have_posts()) : $event_query->the_post();
            global $post; $eventID = $post->ID;
            $event_start_date = get_post_meta($eventID,'event_start',true); 
            $full_date = strtotime($event_start_date);
            $month = date("m",$full_date); 
            // echo $event_month.' ';
            if($event_month) { $search_month = array($event_month); }
            else { $search_month = array(1,2,3,4,5,6,7,8,9,10,11,12); }
            if(in_array($month, $search_month)) { ?>
            <div class="event-single">
               <div class="event-image">
                  <a href="<?php the_permalink(); ?>">
                     <?php if(has_post_thumbnail()) { ?>
                        <?php the_post_thumbnail('medium_large'); ?>
                     <?php } else { ?>
                        <!-- <img src="<?php echo esc_url(plugins_url( '/assets/images/event-placeholder.jpg', __FILE__ )); ?>" > -->
                        <img src="<?php echo THE_EVENT_DEFAULT_EVENT_IMG; ?>" >
                     <?php } ?>
                  </a>
               </div>
               <div class="event-content">
                  <div class="event-title">
                     <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                  </div>
                  <div class="event-desc">
                     <?php the_content(); ?>
                  </div>
               </div>
            </div>
            <?php }
         endwhile; ?>
      </div>
   <?php wp_reset_postdata(); endif;
}
}


if(! function_exists('the_event_post_month_filter')) {
function the_event_post_month_filter() {

   $months = array();
   $i = 0;
   $args = array(
      'post_type' => 'event',
      'post_status' => 'publish',
      'posts_per_page' => -1,
   );
   $query = new WP_Query($args);
   if($query->have_posts()) :
      while($query->have_posts()) : $query->the_post(); 
         global $post; $eventID = $post->ID; 
         $event_start_date = get_post_meta($eventID,'event_start',true); 
         if(! empty($event_start_date)) {
            $full_date = strtotime($event_start_date);
            $month = date("m",$full_date); 
            $months[$i]['month'] = $month;
            $months[$i]['name'] = the_event_post_month__name_filter($month);
            $i++;
         }
      endwhile;
      wp_reset_postdata(); endif;
   return $months;
}
}

if(! function_exists('the_event_post_month__name_filter')) {
function the_event_post_month__name_filter($num) {
   switch($num) {
      case 1:
         $name = 'January';
         break;
      case 2:
         $name = 'February';
         break;
      case 3:
         $name = 'March';
         break;
      case 4:
         $name = 'April';
         break;
      case 5:
         $name = 'May';
         break;
      case 6:
         $name = 'June';
         break;
      case 7:
         $name = 'July';
         break;
      case 8:
         $name = 'August';
         break;
      case 9:
         $name = 'September';
         break;
      case 10:
         $name = 'October';
         break;
      case 11:
         $name = 'November';
         break;
      case 2:
         $name = 'December';
         break;
      default:
         $name = 'January';
         break;
   }
   return $name;
}
}

