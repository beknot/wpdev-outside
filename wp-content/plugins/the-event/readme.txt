=== Event Post ===
Tested up to: 5.8

== Description ==

**Event Post** is a plugin displays event posts with the different post fields like event start date, event end date, event location and etc. **the-event**.


> **Plugin Features**
>
> * Custom Post type (Event) 
> * Filter by event categories, event month
> * Event title, event location, event start, event end  


**Post gird** can be make your website with attractive.

Custom post type for event


== Installation ==

1. Go to the Plugins Menu in WordPress
2. Search for "Post Slider"
3. Click "Install"
4. Go to "Post Carousel" in your admin dashboard
5. Press the "Add new" button 
6. Configure your Setting
7. Press "Update" button and copy & paste your shortcode


== Instruction ==
<h4>How to add Event in Wordpress</h4>
1. Find Event menu in dashboard wordpress
2. Navigate Event menu and create new event
3. Add relevent information related to event and publish
4. use of shortcode to display event


== Usage ==

1. Use of shortcode
	* Display all events according to event category, 
	* descending/ascending, 
	* order by date/

		Example: [event type="webinar" order="DESC" orderby="date" limit="10"]

2. Shortcode filter event
	* Display all events with filterable category and month of event 

	Example: [event_filterable_post]

3. Event psot api

	* SITE_DOMAINI_URL/wp-json/POST_TYPE_NAME/EVENT_CATEGORY/PAGINATION

	Example: http://localhost/wpdev/wp-json/event/webinar/1