<?php

if( ! defined( 'ABSPATH' )) exit;

add_shortcode('event','the_event_list_events');

if(!function_exists('the_event_list_events')) {
function the_event_list_events($attr) {

   $args = shortcode_atts( 
      array(
         'type' => '',
         'order' => 'DESC',
         'orderby' => 'date',
         'limit' => 10,
      ), $attr );

   $event_args = array(
      'post_type' => 'event',
      'post_status' => 'publish',
      'order' => $args['order'],
      'orderby' => $args['orderby'],
      'posts_per_page' => $args['limit'],
   );
   if(! empty($args['type'])) {
      $event_args['tax_query'] = array(
         array(
            'taxonomy' => 'event-type',
            'field' => 'slug',
            'terms' => $args['type'],
         ),
      );
   }
   
   $event_query = new WP_Query($event_args);
   if($event_query->have_posts()) : ?>
      <div class="container">
         <div class="row">
            <?php while($event_query->have_posts()) : $event_query->the_post();
               global $post; $eventID = $post->ID; ?>
               <div class="col-md-4">
                  <div class="event-wrap">
                     <div class="event-image">
                        <a href="<?php the_permalink(); ?>">
                           <?php if(has_post_thumbnail()) { ?>
                              <?php the_post_thumbnail('medium_large'); ?>
                           <?php } else { ?>
                              <!-- <img src="<?php echo esc_url(plugins_url( '/assets/images/event-placeholder.jpg', __FILE__ )); ?>" > -->
                              <img src="<?php echo THE_EVENT_DEFAULT_EVENT_IMG; ?>" >
                           <?php } ?>
                        </a>
                     </div>
                     <div class="event-content">
                        <div class="event-title">
                           <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                        </div>
                        <div class="event-desc">
                           <?php the_content(); ?>
                        </div>
                     </div>
                  </div>
               </div>
            <?php endwhile; ?>
         </div>
      </div>
   <?php wp_reset_postdata(); endif;
}
}