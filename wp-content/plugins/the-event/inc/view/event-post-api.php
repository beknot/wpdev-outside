<?php

if( ! defined( 'ABSPATH' )) exit;

/*
*
* Iniitialize functions call event post and by category
*
*/

include_once dirname( __FILE__ ) . '/api/api-event-all-post.php';

include_once dirname( __FILE__ ) . '/api/api-event-category-post.php';

if(! function_exists('the_event_product_categories')) {
   function the_event_product_categories($postID) {
      $product_cats = get_the_terms($postID,'event-type');
      if( $product_cats ) {
         $cat = array();
         foreach( $product_cats as $product_cat ) {
            $cat[] = $product_cat->term_id;
         }
      }
      $cat_name_arr = the_event_product_category_name_arr($cat);
      return $cat_name_arr;
   }
}

if(! function_exists('the_event_product_category_name_arr')) {
   function the_event_product_category_name_arr($result_arr) {
      if( $result_arr ) {
         $cat_name = array();
         foreach( $result_arr as $result ) {
            $cat__name = get_term_by('id',$result,'event-type');
            $cat_name[] = $cat__name->name;
         }
      }
      return $cat_name;
   }
}


if(! function_exists('the_event_post_categories')) {
function the_event_post_categories() {

   $args = array(
      'taxonomy' => 'event-type',
      'orderby' => 'name',
      'order'   => 'ASC'
   );
   $cats = get_categories($args);
   if($cats) { 
      $i = 0;
      $cat_arr = array();
      foreach($cats as $cat) {
          $cat_arr[$i]['slug'] = $cat->slug;
          $cat_arr[$i]['name'] = $cat->name;
         $i++;
      }
   }
   return $cat_arr;
}
}