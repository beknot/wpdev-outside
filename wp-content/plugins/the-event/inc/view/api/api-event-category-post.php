<?php 

if( ! defined( 'ABSPATH' )) exit;

/* 
*
* Get Event by category
*
* localhost/wpdev/wp-json/event/webinar/1
*
* /wp-json/event/webinar/1
*
*/
add_action( 'rest_api_init', function () {

   register_rest_route( 'event/', '(?P<type>[a-zA-Z0-9-]+)/(?P<paged>[0-9]{1,3})', array(
      'methods' => WP_REST_Server::READABLE,
      'callback' => 'myfunc_get_event_cat',
   ) );
} );

function myfunc_get_event_cat(WP_REST_Request $request) {

   $paged = $request['paged'];
   $type = $request['type'];

   $args = array(
      'post_type'          => 'event',
      'posts_per_page'    => 10,
      'paged'             => $paged,
      'orderby'           => 'date',
      'order'             => 'DESC',
      'tax_query' => array(
         array (
         'taxonomy' => 'event-type',
         'field' => 'slug',
         'terms' => $type,
         )
      ),
   );
   // use WP_Query to get the results with pagination
   $query = new WP_Query( $args ); 

   // if no posts found return 
   if( empty($query->posts) ){
       return new WP_Error( 'no_posts', __('No event found'), array( 'status' => 404 ) );
   }
    
   // set max number of pages and total num of posts
   $max_pages = $query->max_num_pages;
   $total = $query->found_posts;

   $posts = $query->posts; 

   // prepare data for output
   $controller = new WP_REST_Posts_Controller('event');

   $ii = 0;
   foreach ( $posts as $post ) {
      $response = $controller->prepare_item_for_response( $post, $request );

      $postID = $post->ID;
      $data[$ii]['id'] = $post->ID;
      $data[$ii]['title'] = $post->post_title;
      $data[$ii]['content'] = $post->post_content;
      $data[$ii]['featured_media'] = get_the_post_thumbnail_url($post->ID,'medium_large');
      $categories = the_event_product_categories($postID);
      $data[$ii]['categories'] = $categories;
      
      $ii++;
   }


   // set headers and return response      
   $response = new WP_REST_Response($data, 200);

   $response->header( 'X-WP-Total', $total ); 
   $response->header( 'X-WP-TotalPages', $max_pages );

   return $response;
}