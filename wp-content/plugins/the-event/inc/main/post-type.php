<?php 

if( ! defined( 'ABSPATH' )) exit;

add_action( 'init', 'the_event_post_event', 0 );

if ( ! function_exists('the_event_post_event') ) {
  function the_event_post_event() {
    $labels = array(
      'name'                  => _x( 'Event', 'Post Type General Name','the-event' ),
      'singular_name'         => _x( 'Event', 'Post Type Singular Name','the-event' ),
      'menu_name'             => _x( 'Event','the-event' ),
      'all_items'               => __( 'All Events', 'the-event' ),
      'add_new_item'            => __( 'Add Event Order', 'the-event' ),
      'edit_item'               => __( 'Edit Event', 'the-event' ),
      'search_items'            => __( 'Search Event', 'the-event' ),
      'not_found'                   => __( 'No Event found', 'the-event' ),
      'not_found_in_trash'  => __( 'No Event found in Trash', 'the-event' ),
    );
    $rewrite = array(
      'slug'                  => 'event',
      'pages'                 => true,
      'feeds'                 => true,
    );
    $args = array(
      'label'                 => _x( 'Event','the-event' ),
      'labels'                => $labels,
      'supports'              => array( 'title','editor','thumbnail'),
      'public'                => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'has_archive'           => true,    
      'rewrite'               => $rewrite,
      'capability_type'       => 'post',
      'map_meta_cap'    => true,
      'menu_icon'             => 'dashicons-calendar',
    );
    register_post_type( 'Event', $args );
  }
}


add_action( 'init', 'the_event_event_taxonomy', 0 );

if ( ! function_exists('the_event_event_taxonomy') ) {
   function the_event_event_taxonomy() {
      $labels = array(
	      'name'                  => _x( 'Event Type', 'Taxonomy General Name', 'the-event' ),
	      'singular_name'         => _x( 'Event Type', 'Taxonomy Singular Name', 'the-event' ),
	      'menu_name'             => __( 'Event Type', 'the-event' ),
	      'all_items'             => __('All Event Types', 'the-event' ),
	      'add_new_item'          => __( 'Add Event Type', 'the-event' ),
	      'edit_item'             => __( 'Edit Event Type', 'the-event' ),
	      'search_items'          => __( 'Search Event Type', 'the-event' ),
	      'not_found'             => __( 'No Event Type found', 'the-event' ),
	      'not_found_in_trash'    => __( 'No Event Type found in Trash', 'the-event' ),
	    );
	    $rewrite = array(
	      'slug'                  => 'event-type',
	    );
	    $args = array(
	      'labels'                => $labels,
	      'hierarchical'          => true,
	      'public'                => true,
	      'show_admin_column'     => true,
	      'show_in_nav_menus'   => false,
	      'show_in_rest'          => true,
	      'rewrite'               => $rewrite,
	      'capabilities' => array(
	            'manage_terms' => 'edit_users',
	            'edit_terms'   => 'edit_users',
	            'delete_terms' => 'edit_users',
	            'assign_terms' => 'read',
	         ),
	    );
	    register_taxonomy( 'event-type', array( 'event' ), $args );
	}
}