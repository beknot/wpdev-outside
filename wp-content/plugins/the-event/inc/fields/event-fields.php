<?php

if( ! defined( 'ABSPATH' )) exit;

$the_event_whitelist = array('127.0.0.1', "::1");

if(isset($_SERVER['REMOTE_ADDR'])) :
   
if(! in_array(intval($_SERVER['REMOTE_ADDR']), $the_event_whitelist)):

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
   'key' => 'group_618cea1518fdc',
   'title' => 'Events',
   'fields' => array(
      array(
         'key' => 'field_618cea208186d',
         'label' => 'Venue',
         'name' => 'venue',
         'type' => 'text',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
         ),
         'default_value' => '',
         'placeholder' => '',
         'prepend' => '',
         'append' => '',
         'maxlength' => '',
      ),
      array(
         'key' => 'field_618cea588186e',
         'label' => 'Event Start',
         'name' => 'event_start',
         'type' => 'date_time_picker',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
         ),
         'display_format' => 'd/m/Y g:i a',
         'return_format' => 'd/m/Y g:i a',
         'first_day' => 1,
      ),
      array(
         'key' => 'field_618cea7a8186f',
         'label' => 'Event End',
         'name' => 'event_end',
         'type' => 'date_time_picker',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
         ),
         'display_format' => 'd/m/Y g:i a',
         'return_format' => 'd/m/Y g:i a',
         'first_day' => 1,
      ),
      array(
         'key' => 'field_618ceaad77087',
         'label' => 'Entrance',
         'name' => 'entrance',
         'type' => 'text',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
         ),
         'default_value' => '',
         'placeholder' => '',
         'prepend' => '',
         'append' => '',
         'maxlength' => '',
      ),
   ),
   'location' => array(
      array(
         array(
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'event',
         ),
      ),
   ),
   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
   'active' => true,
   'description' => '',
   'show_in_rest' => 0,
));

endif;

endif;

endif; 