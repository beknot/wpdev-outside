<?php

if( ! defined( 'ABSPATH' )) exit;

final class The_event_init {

	/* Plugin version. */
	const VERSION = '1.0';

	protected static $instance = null;

	private function __construct() {

   	if (version_compare('5', get_bloginfo('version'), '<=' )) {
			$this->includes();
		} else {
			add_action( 'admin_notices', array( $this, 'the_event_error_notice' ) );
		}
	}

	private function includes() {

		include_once THE_EVENT_PLUGIN . 'inc/main/post-type.php' ;

		include_once THE_EVENT_PLUGIN . 'inc/view/event-post-code.php';

		include_once THE_EVENT_PLUGIN . 'inc/view/event-post-api.php';

		include_once THE_EVENT_PLUGIN . 'inc/fields/event-fields.php';

	}

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function the_event_error_notice() {
		/* translators: %s: Wordpress version */
		echo '<div class="error notice is-dismissible"><p>' . sprintf( esc_html__( 'Plugin depends on the last version of %s or later to work!', 'the-event' ), esc_html__( 'Wordpress 5.0', 'the-event' ) ) . '</p></div>';
	}
}