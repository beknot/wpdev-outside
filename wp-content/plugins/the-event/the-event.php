<?php
/*
Plugin Name: The Event
Description: Displays the event post types
Author: Binod Chakradhar
Text Domain: the-event
Version: 1.0
*/


if( ! defined( 'ABSPATH' )) exit;


define('THE_EVENT_VERSION','1.0');

define( 'THE_EVENT_TEXT_DOMAIN', 'the-event' );

define( 'THE_EVENT_PLUGIN', plugin_dir_path( __FILE__ ) );

define( 'THE_EVENT_DEFAULT_EVENT_IMG', plugins_url( '/assets/images/event-placeholder.jpg', __FILE__ ));

include_once dirname( __FILE__ ) . '/event-filter/function.php';

add_action( 'wp_enqueue_scripts', 'the_event_scripts' );
function the_event_scripts() {
   wp_enqueue_style( 'the-event-style', plugins_url( '/assets/css/style.css', __FILE__ ), array(), '', '',false );
   wp_enqueue_style( 'the-event-bootstrap', plugins_url( '/assets/css/bootstrap.min.css', __FILE__ ), array(), '', '',false );
   wp_enqueue_script('jquery');
   wp_enqueue_script( 'the-event-custom', plugins_url( '/assets/js/custom.js', __FILE__ ), array(), '', '',true );

   wp_register_script( 'the-event-mainjs', plugins_url( '/assets/js/main.js', __FILE__ ), array(), '', true );

   $translation_array = array(
      'ajax_url' => admin_url( 'admin-ajax.php' )
   );
   wp_localize_script( 'the-event-mainjs', 'the_event_obj', $translation_array );
   wp_enqueue_script( 'the-event-mainjs' );
}

if ( class_exists( 'The_event_init' )) return;

include_once dirname( __FILE__ ) . '/inc/the-event-init.php';

add_action( 'plugins_loaded', array( 'The_event_init', 'get_instance' ) );