jQuery(document).on('click','.month_dot',function() {
   eventMonth = jQuery(this).val();
   isTypeChecked = jQuery('input[name=event_type]:checked').val();
   typeChecked = (isTypeChecked) ? isTypeChecked : '';

   console.log(eventMonth);
   console.log(typeChecked);

   eventType = '';
   
   jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: the_event_obj.ajax_url, 
     data: { action: 'event_fetch', event_type: typeChecked, event_month: eventMonth },
      beforeSend: function() {
      },
      success: function(response) {
         jQuery('.event-display').empty();
         jQuery('.event-display').append( response.event_filter_data);

      },
      complete: function() {
      }
   });
});

jQuery(document).on('click','.type_dot',function() {
   eventType = jQuery(this).val();
   isMonthChecked = jQuery('input[name=event_month]:checked').val();
   monthChecked = (isMonthChecked) ? isMonthChecked : '';

   console.log(eventType);
   console.log(monthChecked);

   jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: the_event_obj.ajax_url, 
     data: { action: 'event_fetch', event_type: eventType, event_month: monthChecked },
      beforeSend: function() {
      },
      success: function(response) {
         jQuery('.event-display').empty();
         jQuery('.event-display').append( response.event_filter_data);

      },
      complete: function() { console.log('done');
      }
   });
});